create database timelessMedicalEmployee;

use timelessMedicalEmployee;

create table EmployeeMaster
( EmployeeID int unsigned not null auto_increment primary key,
  EmployeeFirstName char(50) not null,
  EmployeeLastName char(50) not null,
  EmployeeBday date not null
);
create table DTRMaster
(  DTREmployeeID char(13) not null primary key,
   DTRDateTime datetime,
   DTRType char(10) ,
   DTRTotalHours int not null
);






