
<?php
//This will redirect the user to the login page if the are not logged in the system
require_once('authenticateLogin.php');
?>
<!--
Author:Charles Aondo
Date:2019-07-07
Purpose:This is the main to view all the employees that are stored in the database
-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee List</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body class="backgroundDesign">
    <div id="Container">
        <?php
            session_start();
            //            Connecting to the database
            @$myDb = new mysqli('localhost', 'root', '', 'timelessMedicalEmployee');
            //Redirect the user to the home page if database connection fails
            if(mysqli_connect_error()){
                echo "Could not connect to the database";
                header("location:index.php");
                die("</body></html>");
            }else {
    //            Querying the database for all the users information
                $query= "SELECT * FROM employeemaster
                        LEFT JOIN dtrmaster ON employeemaster.EmployeeID = dtrmaster.DTREmployeeID
                        UNION
                        SELECT * FROM employeemaster
                        RIGHT JOIN dtrmaster ON employeemaster.EmployeeID = dtrmaster.DTREmployeeID";

                $searchResult = $myDb->query($query);

    //Displaying the result in a tabular form
                echo '<table class="table table-striped table-bordered">';
                echo '<tr>';
                echo '<th>Employee ID</th>';
                echo '<th>First Name</th>';
                echo '<th>Last Name</th>';
                echo '<th>Employee DOB</th>';
                echo '<th>DTR Employee ID</th>';
                echo '<th>DTRType</th>';
                echo '<th>DTRDateTime</th>';
                echo '<th>Total Hours</th>';
                echo '<th>Delete</th>';
                echo '<th>Edit</th>';

                if ($searchResult->num_rows > 0) {
                    //Looping through all the employee data in the database to display on the table
                    while ($row = $searchResult->fetch_assoc()) {
                        echo '</tr>';
                        echo '<td>' . $row['EmployeeID'] . '</td>';
                        echo '<td>' . $row['EmployeeFirstName'] . '</td>';
                        echo '<td>' . $row['EmployeeLastName'] . '</td>';
                        echo '<td>' . $row['EmployeeBday'] . '</td>';
                        echo '<td>' . $row['DTREmployeeID'] . '</td>';
                        echo '<td>' . $row['DTRDateTime'] . '</td>';
                        echo '<td>' . $row['DTRType'] . '</td>';
                        echo '<td>' . $row['DTRTotalHours'] . '</td>';
                        echo '<td> <a href="delete.php?EmployeeID= ' . $row['EmployeeID'] . '">Delete</a>
               
                                    </td>';
                        echo '<td>
                                   <a href="edit.php?EmployeeID=' . $row['EmployeeID'] . '">Edit</a>
                                    </td>';
                    }
                    echo '</table>';
                }
            }

        ?>
    </div>
</body>
</html>