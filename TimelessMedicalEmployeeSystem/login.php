<!--
Author:Charles Aondo
Date:2019-07-07
Purpose:A logging page that accepts users logging credentials and logged they in using session if their
credentials are authenticated and are matched.
-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login User</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body id="loginBackground">
<!--Forms that accepts users login-->
    <div id="Container">
        <form method="get" action="login.php">
            <h1>Password = 123, username = admin</h1>
            <div class="form-group">
                <label>User Name</label>
                <input class="form-control" name="username" type="text">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" name="pass" type="password">
            </div>
            <button type="submit" name="login" class="btn btn-primary">Login</button>
        </form>
        <?php
//        Start Session
        session_start();
//            Initialising the session variable
            $_SESSION["isLoggedIn"] = false;

            //Validating the data above to enable a user login
            if(isset($_GET['login'])){
                //Stripping away any js that may be attached to login, no further security required since login info are not ]
                //been stored into the database
                $userName = htmlspecialchars($_GET['username']);
                $password = htmlspecialchars($_GET['pass']);
                //Validating a successful login
                if ($userName == "admin" && $password == "123") {
//                    session_regenerate_id();
                    $_SESSION["username"]  = $userName;
                    $_SESSION["isLoggedIn"]=true;
                    header("location:ViewEmployees.php");
                    echo '<h2 class="passColor">Login Successful</h2>';

                } else{
                    echo '<h2 class="error">Incorrect Username/Password</h2>';
                }
            }
        ?>
    </div>
</body>
</html>