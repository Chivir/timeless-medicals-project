<?php
require_once ('authenticateLogin.php');
//    Connecting to the database
    @$myDb = new mysqli('localhost', 'root', '', 'timelessMedicalEmployee');
//    Getting the  isbn from the url to store it into the database
    $employeeID = $_GET['EmployeeID'];
    if(empty($employeeID)){
        header('location:index.php');
        die();
    }
    //Deleting the user from the both tables in DB
    mysqli_real_escape_string($myDb,$employeeID);
    //a , b are both aliases
    $query ="DELETE a.*, b.* FROM EmployeeMaster a
                          INNER JOIN DTRMaster b ON a.EmployeeID = b.DTREmployeeID
                          WHERE a.EmployeeID='$employeeID'";
    $result = $myDb->query($query);
    //Informing user outcome of the the operation
    if($result->num_rows>0){
        echo '<h2 class="passColor">Delete Successful</h2>';
        header('location:index.php');
        die();
    }
    $stmt->free_result();
    $myDb->close(); 
?>
