<!--
Author:Charles Aondo
Date:2017-07-07
Purpose:This page collects the data posted from the addemployee page,clean the data and inserts its into the database
-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adding Employee to Database</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body id="loginBackground">
    <div id="Container">
        <?php
        //Starting session
        session_start();
        //Creating a session variable
        $_SESSION["isLoggedInEmployee"]= false;
//     Redirecting the user to the home add employee if the get here without going to the employee sign Page
        //Function that sanitizies the data entered by the user

            if(!isset($_GET['submit'])){
                header("location:employeeSignIn.php");
                die("</body></html>");
            }
        if(isset($_GET['submit'])){
            //The following code below is setup this way in order to avoid using post for data validation
            //Connecting to the database
            @$DB = new mysqli('localhost', 'root', '', 'timelessMedicalEmployee');
            //Checking for errors in database connection
            if(mysqli_connect_error()){
                echo '<h2 class="error">Database Error!! System is Down, Please Report to a Supervisor to Clock In!<a href="addEmployee.php"></a></h2>';
                die("</body></html>");
            }
            //Using mysql real escape string to escaped malicious user data
            $employeeID = mysqli_real_escape_string($DB,$_GET['id']);


//                //Validating the fields for empty data, it is a good practice not to rely on html required attribute for data validation your user data
                if(empty($employeeID)){
                    echo '<h2><a href="executeEmployeeSign.php" class="error">Empty field! Follow this link to Sign In</a></h2>';
                }
                //Checking the db to see if the employee signing in is a valid employee with the company
                    else{
                        $query = "SELECT * FROM DTRMaster WHERE DTREmployeeID='$employeeID'";
                        $result = mysqli_query($DB, $query);
                        $count  = mysqli_num_rows($result);
                    }
                    //Employee has successfully signed in,time to start calculating hours
                    if($count == 1){
                        echo '<h2 class="passColor">Successfully Clocked In, Have a great Day</h2>'.'<br>';
                        $_SESSION["isLoggedInEmployee"] = true;
                    }else{
                        echo '<h2>Fatal Error!! Retry or Contact a supervisor</h2>';
                        echo '<h2><a href="employeeSignIn.php">Retry</a></h2>'.'<br>';
                    }
                     if(isset($_SESSION["isLoggedInEmployee"]) == true) {
                        //Getting and setting the time zone to be inserted into the DB

                         date_default_timezone_set("America/Halifax");
                         $dateTime =  date("Y-m-d H:i:s");

                        //Setting timer to calculate the time the user has been working for
                         $_SESSION ['timer'] = time();
                         $_SESSION['loginTime'] = new DateTime(date('y-m-d h:m:s'));
                         $status ="In";
                         //Updating the database with the time and date the user clocked in and the DTR type
                         $stmt = $DB->prepare("UPDATE  DTRMaster SET DTRDateTime=?,DTRType = ? WHERE DTREmployeeID = ?");
                         $stmt->bind_param("sss",$dateTime,$status,$employeeID);
                         $stmt->execute();
                         header("location:clockout.php");
                         die("</body></html>");
                     }
                    //Free and close all database resources
                    $result->free_result();
                    $DB->close();
            }

        ?>
    </div>
</body>
</html>