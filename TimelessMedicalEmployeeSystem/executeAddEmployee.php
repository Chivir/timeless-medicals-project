<!--
Author:Charles Aondo
Date:2017-07-07
Purpose:This page collects the data posted from the addemployee page,clean the data and inserts its into the database
-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adding Employee to Database</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body id="loginBackground">
    <div id="Container">
        <?php
//     Redirecting the user to the home add employee if the get here without going to the add employee  page
            if(!isset($_GET['submit'])){
                header("location:addEmployee.php");
                die("</body></html>");
            }

        if(isset($_GET['submit'])){
            //The following code below is setup this way in order to avoid using post for data validation
            //Connecting to the database
            @$DB = new mysqli('localhost', 'root', '', 'timelessMedicalEmployee');
            //Checking for errors in database connection
            if(mysqli_connect_error()){
                echo '<h2 class="error">Could not connect to the database!<a href="addEmployee.php">Click here to go back</a></h2>';
                die("</body></html>");
            }
            //Using mysql real escape string to escaped malicious user data
            $employeeID = mysqli_real_escape_string($DB,$_GET['id']);
            $employeeLastName = mysqli_real_escape_string($DB,$_GET['lastName']);
            $employeeFirstName = mysqli_real_escape_string($DB,$_GET['firstName']);
            $employeeBday = mysqli_real_escape_string($DB,$_GET['DOB']);

//                //Validating the fields for empty data, it is a good practice not to rely on html required attribute for data validation your user data
                if(empty($employeeID && $employeeLastName && $employeeFirstName && $employeeBday)){
                    echo '<h2><a href="addEmployee.php" class="error">Empty field! Follow this link to go and fill the form</a></h2>';
                }
                    else{
                        //Using the prepared statement to insert data into the database,
                        //Inserting the data collected and santizied from above into the database
                        $stmt = $DB->prepare("INSERT INTO EmployeeMaster(EmployeeID, EmployeeFirstName, EmployeeLastName, EmployeeBday) VALUES (?,?,?,?)");
                        $stmt->bind_param("ssss",$employeeID,$employeeFirstName,$employeeLastName,$employeeBday);
                        $stmt->execute();

                        //Inserting into the DTRMaster
                        $stmt1 = $DB->prepare("INSERT INTO DTRMaster(DTREmployeeID) VALUES (?)");
                        $stmt1->bind_param("s",$employeeID);
                        $stmt1->execute();

                    }
                    //Displaying the status of the update to the user
                    if($stmt->affected_rows >0 || $stmt1->affected_rows >0 ){
                        echo '<h2 class="passColor">Employee Successfully Inserted in the Database</h2>'.'<br>';
                        echo '<h2><a href="addEmployee.php">Add another Employee</a></h2>'.'<br>';
                        echo '<h2><a href="index.php">Home</a></h2>';
                    }else{
                        echo '<h2>Fatal Error!! Could not Add into the Database</h2>';
                        echo '<h2><a href="addEmployee.php">Retry</a></h2>'.'<br>';
                        echo '<h2><a href="index.php">Home</a></h2>';
                    }
                    //Free and close all database resources
                    $stmt->free_result();
                    $DB->close();
            }

        ?>
    </div>
</body>
</html>